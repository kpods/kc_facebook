Pod::Spec.new do |s|
  s.name         = 'KC_Facebook'
  s.version      = '1.0.3'
  s.summary      = 'Facebook SDK 4.0.1 without AdSupport.framework - by Kennix'
  s.platform     = :ios, "7.0"
  s.homepage = 'https://bitbucket.org/kcpod/kc_facebook'
  s.license = 'MIT'
  s.author = {
    'Kennix' => 'kennix4309@gmail.com'
  }
  s.source = {
    :git => 'https://kennix426@bitbucket.org/kcpod/kc_facebook.git',
    :tag => '1.0.3'
  }
  s.source_files = 'kc_fb/KC_FacebookSDK/*','kc_fb/KC_FacebookSDK/*/*','kc_fb/KC_FacebookSDK/*/*/*','kc_fb/KC_FacebookSDK/*/*/*/*','kc_fb/KC_FacebookSDK/*/*/*/*/*'
  s.weak_frameworks = "Accounts", "CoreLocation", "Social", "Security", "QuartzCore", "CoreGraphics", "UIKit", "Foundation", "AudioToolbox"
  s.dependency 'Bolts', '~> 1.3.0'


end
